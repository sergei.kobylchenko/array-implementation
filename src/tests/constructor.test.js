import MyArray from '../index';

describe('tests for constructor of class', () => {
  test('should correct work with 0,1,2 ... n arguments', () => {
    const arr = new MyArray();
    const arr1 = new MyArray(1);
    const arr2 = new MyArray(1, 2);

    expect(arr).toHaveLength(0);
    expect(arr[0]).toBeUndefined();

    expect(arr1).toHaveLength(1);
    expect(arr1[0]).toBeUndefined();

    expect(arr2).toHaveLength(2);
    expect(arr2[0]).toBe(1);
    expect(arr2[1]).toBe(2);
  });

  test('the length of the instance should not be enumerable', () => {
    const arr = new MyArray(1, 2, 3);

    expect(
      Object.prototype.propertyIsEnumerable.call(arr, 'length')
    ).toBeFalsy();
  });

  test('each instance should be a new instance', () => {
    const arr1 = new MyArray(2);
    const arr2 = new MyArray(2);

    expect(arr1).not.toBe(arr2);
  });

  test('should return empty array with length equal to argument value, if called with 1 argument', () => {
    const arr = new MyArray(2);

    expect(arr).toHaveLength(2);
    expect(arr[0]).toBeUndefined();
    expect(arr[1]).toBeUndefined();
    expect(arr[2]).toBeUndefined();
  });

  test('should throw an error if length in not positive integer', () => {
    expect(() => {
      const arr = new MyArray(NaN);
    }).toThrowError('Invalid array length');

    expect(() => {
      const arr = new MyArray(Infinity);
    }).toThrowError('Invalid array length');

    expect(() => {
      const arr = new MyArray(-5);
    }).toThrowError('Invalid array length');

    expect(() => {
      const arr = new MyArray(1.5);
    }).toThrowError('Invalid array length');
  });
});
