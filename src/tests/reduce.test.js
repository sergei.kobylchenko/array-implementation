import MyArray from '../index';

describe('tests for reduce()', () => {
  test('should execute a callback function on each element of the array if provided a second argument', () => {
    const arr = new MyArray(1, 2, 5);
    const mockCallback = jest.fn();
    arr.reduce(mockCallback, 10);
    expect(mockCallback.mock.calls).toHaveLength(3);
  });

  test(`accumulator should be equal a second argument in first-time cb
  calling if method was provided a second argument (initial value)`, () => {
    const arr = new MyArray(1, 2, 5);
    const mockCallback = jest.fn();
    arr.reduce(mockCallback, 10);
    expect(mockCallback.mock.calls[0][0]).toBe(10);
  });

  test(`should execute a callback on each element of the
  array except first if does not provided a second argument`, () => {
    const arr = new MyArray(1, 2, 5);
    const mockCallback = jest.fn();
    arr.reduce(mockCallback);
    expect(mockCallback).toHaveBeenCalledTimes(2);
  });

  test('accumulator will be equal array first element if method does not provided a second argument.', () => {
    const arr = new MyArray(1, 2, 5);
    const mockCallback = jest.fn();
    arr.reduce(mockCallback);
    expect(mockCallback.mock.calls[0][0]).toBe(1);
  });

  test('should return initialValue without calling a callback, if arr is empty and initialValue is provided', () => {
    const arr = new MyArray();
    const mockCallback = jest.fn();
    const sum = arr.reduce(mockCallback, 10);
    expect(sum).toBe(10);
    expect(mockCallback.mock.calls).toHaveLength(0);
  });

  test(`should return an array element without callback
  if there is one element in the array and without initialValue`, () => {
    const arr = new MyArray('1');
    const mockCallback = jest.fn();
    const sum = arr.reduce(mockCallback);
    expect(sum).toBe('1');
    expect(mockCallback.mock.calls).toHaveLength(0);
  });

  test('callback should take 4 arg (accumulator, value, index, array)', () => {
    const arr = new MyArray(1, 2, 3);
    const mockCallback = jest.fn();
    arr.reduce(mockCallback);
    expect(mockCallback).toBeCalledWith(1, 2, 1, arr);
  });

  test('should throw an error if array is empty and without initial value', () => {
    const arr = new MyArray();
    const mockCallback = jest.fn();
    expect(() => {
      arr.reduce(mockCallback);
    }).toThrowError('Reduce of empty array with no initial value');
  });

  test('method should have minimum one argument', () => {
    const arr = new MyArray(1, 2, 3);
    expect(arr.reduce).toHaveLength(1);
  });
});
