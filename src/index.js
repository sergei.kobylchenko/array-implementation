const handler = {
  get(target, prop, receiver) {
    return target[prop];
  },
  set(target, prop, value) {
    if (prop !== 'length') {
      if (!(prop in target)) {
        target.length =
          prop >= target.length ? Number(prop) + 1 : target.length;
      }

      target[prop] = value;
    }

    if (prop === 'length') {
      if (Number.isInteger(Number(value)) && value < 2 ** 32) {
        if (target[prop] > value) {
          const currentLen = target.length;

          for (let i = currentLen - 1; i >= value; i -= 1) {
            delete target[i];
            target.length -= 1;
          }
        }
        target[prop] = Number(value);
      } else {
        throw new Error('Invalid array length');
      }
    }

    return true;
  }
};

class MyArray {
  constructor(...args) {
    if (args.length === 0) {
      this.length = 0;
    } else if (args.length === 1 && typeof args[0] === 'number') {
      if (args[0] < 0 || !Number.isInteger(args[0])) {
        throw new Error('Invalid array length');
      } else {
        this.length = args[0];
      }
    } else {
      for (let i = 0; i < args.length; i += 1) {
        this[i] = args[i];
        this.length = i + 1;
      }
    }
    Object.defineProperty(this, 'length', {
      enumerable: false,
      writable: true
    });

    return new Proxy(this, handler);
  }

  [Symbol.iterator]() {
    const obj = this;
    let count = 0;

    return {
      next() {
        if (count === obj.length) {
          return { value: undefined, done: true };
        }
        count += 1;
        return { value: obj[count - 1], done: false };
      }
    };
  }

  push(...args) {
    for (const key of args) {
      this[this.length] = key;
    }
    return this.length;
  }

  pop() {
    if (this.length > 0) {
      const indx = this.length - 1;
      const el = this[indx];
      delete this[indx];
      this.length -= 1;
      return el;
    }
  }

  shift() {
    const el = this[0];
    const { length } = this;

    if (length > 0) {
      for (let i = 1; i < length; i += 1) {
        if (i in this) {
          this[i - 1] = this[i];
        }
      }
      delete this[length - 1];
      this.length -= 1;
    }
    return el;
  }

  unshift(el, ...args) {
    if (el === undefined) {
      return this.length;
    }

    const items = new MyArray(el, ...args);
    const delta = args.length + 1;
    this.length = this.length + delta;

    for (let i = this.length - 1; i >= 0; i -= 1) {
      if (i in this) {
        this[i + delta] = this[i];
        delete this[i];
      }

      if (i < delta) {
        this[i] = items[i];
      }
    }
    return this.length;
  }

  static from(value, ...args) {
    const array = new MyArray();
    const mapFn = args[0];
    const fnThis = args[1];

    if (typeof mapFn !== 'undefined' && typeof mapFn !== 'function') {
      const errorMsg = `${mapFn} is not a function. When provided, the second argument must be a function`;
      throw new TypeError(errorMsg);
    }

    if (value === null || typeof value === 'undefined') {
      const errorMsg =
        'myArray.from requires an array-like object - not null or undefined';
      throw new TypeError(errorMsg);
    }

    if (typeof value === 'number') {
      return array;
    }

    for (let i = 0; i < value.length; i += 1) {
      if (mapFn) {
        array[i] = fnThis ? mapFn.call(fnThis, value[i]) : mapFn(value[i]);
      } else {
        array[i] = value[i];
      }
    }
    return array;
  }

  map(cbFn, ...args) {
    const array = new MyArray();
    const fnThis = args[0];

    for (let i = 0; i < this.length; i += 1) {
      if (i in this) {
        if (fnThis) {
          array[i] = cbFn.apply(fnThis, new MyArray(this[i], i, this));
        } else {
          array[i] = cbFn(this[i], i, this);
        }
      }
    }
    return array;
  }

  forEach(cbFn, ...args) {
    const fnThis = args[0];
    const { length } = this;

    for (let i = 0; i < length; i += 1) {
      if (i in this) {
        cbFn.call(fnThis, this[i], i, this);
      }
    }
  }

  reduce(cbFn, ...args) {
    const initialValue = args[0];
    let i = 0;

    /* check if array is empty */
    while (i < this.length && !(i in this)) {
      i += 1;
    }

    if (i >= this.length && initialValue === undefined) {
      throw new TypeError('Reduce of empty array with no initial value');
    }
    /* */

    let acc = initialValue || this[i];

    if (acc !== initialValue) {
      i += 1;
    }

    for (; i < this.length; i += 1) {
      if (i in this) {
        acc = cbFn(acc, this[i], i, this);
      }
    }
    return acc;
  }

  filter(cbFn, ...args) {
    const array = new MyArray();
    const { length } = this;
    const fnThis = args[0];

    for (let i = 0; i < length; i += 1) {
      if (i in this) {
        if (cbFn.call(fnThis, this[i], i, this)) {
          array[array.length] = this[i];
        }
      }
    }
    return array;
  }

  sort(inputFn) {
    const compareFn =
      inputFn ||
      function(a, b) {
        if (String(a) > String(b)) {
          return 1;
        }

        if (String(a) === String(b)) {
          return 0;
        }

        if (String(a) < String(b)) {
          return -1;
        }
      };

    let k = 0;

    for (let m = 0; m < this.length; m += 1) {
      if (!(m in this)) {
        k += 1;
      }
    }

    const count = this.length - 1;

    for (let i = 0; i < count; i += 1) {
      for (let j = 0; j < count - i; j += 1) {
        if (
          !(this[j + 1] === undefined) &&
          (this[j] === undefined || compareFn(this[j], this[j + 1]) > 0)
        ) {
          const temp = this[j];
          this[j] = this[j + 1];
          this[j + 1] = temp;
        }
      }
    }

    if (k > 0) {
      for (; k > 0; k -= 1) {
        delete this[this.length - k];
      }
    }

    return this;
  }

  toString() {
    let resultString = '';

    for (let i = 0; i < this.length; i += 1) {
      if (i === this.length - 1) {
        resultString += i in this ? `${String(this[i])}` : '';
      } else {
        resultString +=
          i in this && !(this[i] === null || this[i] === undefined)
            ? `${String(this[i])},`
            : ',';
      }
    }
    return resultString;
  }

  slice(a, b) {
    const resultArray = new MyArray();

    if (!(typeof b === 'undefined' || Number.isInteger(Number(b)))) {
      return resultArray;
    }

    let start = Number(a) || 0;
    let end = Number(b) || this.length;

    if (start < 0) {
      start = this.length + start;
    }

    if (end < 0) {
      end = this.length + end;
    }

    for (let i = 0; i < end - start; i += 1) {
      resultArray[i] = this[start + i];
    }
    return resultArray;
  }

  splice(a, b, ...args) {
    const resultArray = new MyArray();
    const { length } = this;

    const start = Number(a) < 0 ? length + Number(a) : Number(a);
    const deleteCount = Number(b) < 0 ? 0 : Number(b);

    if (
      (a === 0 && b === 0) ||
      (a === undefined && b === undefined) ||
      start > length
    ) {
      return resultArray;
    }

    const end = start + deleteCount < length ? start + deleteCount : length;

    // push elements to result array
    for (let i = start; i < end; i += 1) {
      resultArray[i - start] = this[i];
      delete this[i];
    }

    for (let i = start; i < length; i += 1) {
      this[i] = this[i + deleteCount];
    }

    this.length -= end - start;

    if (args.length > 0) {
      const tempArr = Object.assign(new MyArray(), this);
      const tempArrLength = tempArr.length + args.length;

      for (let i = start; i < tempArrLength; i += 1) {
        if (i <= args.length) {
          this[i] = args[i - start];
        } else {
          this[i] = tempArr[i - args.length];
        }
      }
    }
    return resultArray;
  }
}

export default MyArray;
